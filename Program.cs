﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine(Liida(4, 7));
        }

        Console.WriteLine("Hei sisesta üks number: ");
        var i1 = Console.ReadLine();
        Console.WriteLine($"Sa sisestasid numbri: {i1}");


        /// <summary>
        /// see on liitmise funktsioon
        /// </summary>
        /// <param name="yks">Esimene liidetav</param>
        /// <param name="teine">Teine liidetav</param>
        /// <returns></returns>
        static int Liida(int yks, int teine)
        {
            return yks + teine;
        }
    }
}
